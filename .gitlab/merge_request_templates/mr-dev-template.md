# Gitlab MR Web Template

## Descrição

Insira aqui informações sobre a sua issue, o que ela visa resolver de problema, as bibliotecas
adicionadas, se houver, e quais métodos foram necessários durante o seu desenvolvimento.

Se ocorreu alguma breaking change liste ela aqui também, e aponte o que deve ser feito para se
adaptar a mudança.

Drafts: Colocar o que deve ser finalizado antes do merge

Fixes #(numero da issue)

## Tempo de desenvolvimento

Insira aqui o tempo estimado para a tarefa, e após, o quanto foi gasto na realização das tarefas.

Veja como inserir de forma correta em
[gitlab docs - time tracking](https://docs.gitlab.com/ee/user/project/time_tracking.html).

## Tipo de mudança

- [ ] Bug fix (Correção de um problema que não quebrava algo grande)
- [ ] New feature (Mudança que adiciona novas funcionalidades)
- [ ] Breaking change (correção ou recurso que faria com que a funcionalidade existente não
      funcionasse como esperado)
- [ ] Refactor (Alteração que melhora algo que já existente)

# Em quais navegadores foi testado?

- [ ] Google Chrome
- [ ] Mozilla Firefox
- [ ] IE 11 (Windows 10)
- [ ] Edge
- [ ] Opera
- [ ] Opera GX
- [ ] IOS Safari
- [ ] IOS Chrome
- [ ] Android Chrome
- [ ] Android Firefox

# Testei as seguintes resoluções no navegador:

- [ ] 2520 x 1080
- [ ] 1920 x 1080
- [ ] 1400 x 790
- [ ] 1280 x 720
- [ ] 1024 x 1366 (iPad Pro vertical)
- [ ] 1366 x 1024 (iPad Pro horizontal)
- [ ] 768 x 1024 (iPad vertical)
- [ ] 1024 x 768 (iPad horizontal)
- [ ] 375 x 812 (iPhone X)
- [ ] 320 x 568 (iPhone 5/SE)

# Checklist:

## Qualidade

- [ ] Segui os padrões de criação da stack em svelte
- [ ] Eu revisei ou reli o meu próprio código
- [ ] Segui o formato de criação e nomenclatura de variáveis CSS

## Documentação

- [ ] Documentei onde era mais necessário e difícil de entender no meu código
- [ ] Documentei as variáveis CSS que meu componente externaliza
- [ ] Criei bons exemplos no storybook para meu componente

## Teste

- [ ] As mudanças que esta MR implementa possui testes automatizados
- [ ] Executei e garanto que os testes automatizados já criados estão todos passando
- [ ] Executei e garanto que não há erros de lint
- [ ] Executei e garanto que não há erros de typescript

## MR

- [ ] Eu conferi a checklist de atividades, e expliquei no meu merge request caso algo não tenha
      sido necessário ou possível
